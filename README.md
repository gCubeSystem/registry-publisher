# registry-publisher

In conjunction with the IC-Client, the Registry-Publisher represents the mediation layer gCube Services, based on FWS, will rely on to interact with the Information Service as a whole.


## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management


## Documentation

See registry-publisher on [Wiki](https://gcube.wiki.gcube-system.org/gcube/Registry-Publisher).

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **John Doe** ([ORCID](https://orcid.org/...-...)) - *Initial work*  @ [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)
* **Fabio Simeoni** (fabio.simeoni-AT-fao.org), FAO of the UN, Italy
* **Lucio Lelii** (lucio.lelii-AT-isti.cnr.it), CNR, Italy 
* **Roberto Cirillo** (roberto.cirillo-AT-isti.cnr.it), ISTI-CNR.

## How to Cite this Software

Tell people how to cite this software. 
* Cite an associated paper?
* Use a specific BibTeX entry for the software?


    @Manual{,
        title = {.. registry-publisher ..},
        author = {{Infrascience Group}},
        organization = {ISTI - CNR},
        address = {Pisa, Italy},
        year = 2019,
        note = {...},
        url = {http://www.http://gcube-system.org/}
    } 

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
 
 * Hat tip to anyone whose code was used
 * Inspiration
 * etc

