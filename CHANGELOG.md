# Changelog for "registry-publisher"

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.3.2] 2020-07-26
### Fixes
- fix updateVo method. if a generic resource was updated by monitor at root-vo level, the change wasn't propagated properly to the others VOs (#19690)



## [v1.3.1] 2020-12-10

### Fixes
 - fix getInternalVOScopes method. Now it returns only the VO and root-vo as well
 
## [v1.3.0] 2019-09-04
### New Features
Added create and remove operation at vo level
Resource synchronization with the more recent one searched at VO level
create, remove operation fail only if there is a problem in the current VO

